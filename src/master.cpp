#include <any>

#include "pubsub/master.h"

namespace PS {

Master::Master()
{
}

Master* Master::GetInstance()
{
  if (m_Instance == nullptr) 
  {
    m_Instance = new Master();
  }

  return m_Instance;
}

void Master::RegisterPublisher(const std::string& topic, Publisher<std::any> *publisher)
{
  std::list<Publisher<std::any>*> publishers;
  bool new_topic = m_PublisherMap.find(topic) == m_PublisherMap.end();

  if (!new_topic)
    publishers = m_PublisherMap.at(topic);
  else
  {
    // Add topic to subscribers list too
    std::list<Subscriber<std::any>*> subscribers;
    m_SubscriberMap.insert_or_assign(topic, subscribers);
  }

  publishers.push_back(publisher);

  if (new_topic)
    m_PublisherMap.insert_or_assign(topic, publishers);
}

void Master::UnregisterPublisher(const std::string& topic, Publisher<std::any> *publisher)
{
  assert(m_PublisherMap.find(topic) != m_PublisherMap.end());

  auto publishers = m_PublisherMap.at(topic);
  publishers.remove(publisher);
}

void Master::RegisterSubscriber(const std::string& topic, Subscriber<std::any> *subscriber)
{
  std::list<Subscriber<std::any>*> subscribers;
  bool new_topic = m_SubscriberMap.find(topic) == m_SubscriberMap.end();

  if (!new_topic)
    subscribers = m_SubscriberMap.at(topic);
  else
  {
    // Add topic to subscribers list too
    std::list<Publisher<std::any>*> publishers;
    m_PublisherMap.insert_or_assign(topic, publishers);
  }

  subscribers.push_back(subscriber);

  if (new_topic)
    m_SubscriberMap.insert_or_assign(topic, subscribers);
}

void Master::UnregisterSubscriber(const std::string& topic, Subscriber<std::any> *subscriber)
{
  assert(m_SubscriberMap.find(topic) != m_SubscriberMap.end());

  auto subscribers = m_SubscriberMap.at(topic);
  subscribers.remove(subscriber);
}

std::list<Subscriber<std::any>*> Master::GetSubscribersToTopic(const std::string& topic)
{
  std::list<Subscriber<std::any>*> subscribers;
  // Only do this if the topic is advertised and there are subscribers
  if (m_PublisherMap.find(topic) == m_PublisherMap.end() || m_SubscriberMap.find(topic) == m_SubscriberMap.end())
  {
    return subscribers;
  }
  subscribers = m_SubscriberMap.at(topic);
  return subscribers;
}

}