#pragma once

#include <string>
#include <type_traits>

#include "pubsub/master.h"

namespace PS
{

template <typename T>
class Subscriber
{
  friend class Master;

 private:
  std::string m_Topic;
  void (*m_Callback)(T *data);

  void Subscribe()
  {
    Master *master = Master::GetInstance();
    master->RegisterSubscriber(m_Topic, static_cast<void*>(this));
  }

  void Unsubscribe()
  {
    Master *master = Master::GetInstance();
    master->UnregisterSubscriber(m_Topic, static_cast<void*>(this));
  }

  void HandleMessage(T *data)
  {
    m_Callback(data);
  }

 public:
  Subscriber(const std::string& topic, void (*callback)(T *data), bool subscribe_immediately = true)
    : m_Topic(topic), m_Callback(callback)
  {
    if (subscribe_immediately)
    {
      Subscribe();
    }
  }
  ~Subscriber() = default;
};

}