#pragma once

#include <any>
#include <list>
#include <unordered_map>
#include <string>

template <typename T>
class Publisher;

template <typename T>
class Subscriber;

namespace PS
{

class Master
{
 private:
  static Master *m_Instance;
  Master();
  ~Master() = default;

  std::unordered_map<std::string, std::list<Publisher<std::any>*>> m_PublisherMap;
  std::unordered_map<std::string, std::list< Subscriber<std::any>*>> m_SubscriberMap;

 public:
  static Master *GetInstance();

  void RegisterPublisher(const std::string& topic, Publisher<std::any> *publisher);
  void UnregisterPublisher(const std::string& topic, Publisher<std::any> *publisher);

  void RegisterSubscriber(const std::string& topic, Subscriber<std::any> *subscriber);
  void UnregisterSubscriber(const std::string& topic, Subscriber<std::any> *subscriber);
  
  std::list<Subscriber<std::any>*> GetSubscribersToTopic(const std::string& topic);
};

}
