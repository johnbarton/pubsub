#pragma once

#include <string>
#include <type_traits>

#include "pubsub/master.h"

namespace PS
{

template <typename T>
class Publisher
{
  friend class Master;
  
 private:
  std::string m_Topic;

 public:
  Publisher(const std::string& topic, bool advertise_immediately = true)
    : m_Topic(topic)
  {
    if (advertise_immediately)
    {
      Advertise();
    }
  }
  virtual ~Publisher() {}

  // TODO: Can the untemplated implementations be placed in CPP file?
  void Advertise()
  {
    Master *master = Master::GetInstance();
    // Publisher<std::any> *casted = static_cast<void*>(this);
    Publisher<std::any>* casted{static_cast<Publisher<std::any>*>(this)};
    master->RegisterPublisher(m_Topic, casted);
  }

  void Unadvertise()
  {
    Master *master = Master::GetInstance();
    // Publisher<std::any> *casted = static_cast<void*>(this);
    Publisher<std::any>* casted{static_cast<Publisher<std::any>*>(this)};
    master->UnregisterPublisher(m_Topic, casted);
  }

  void Publish(T *data)
  {
    Master *master = Master::GetInstance();
    // master->PublishData(m_Topic, (void*) data);

    auto subscribers = master->GetSubscribersToTopic(m_Topic);
    for (auto& subscriber : subscribers)
    {
      Subscriber<T> sub = std::any_cast<Subscriber<T>>(subscriber);
      
      subscriber->HandleMessage(data);
    }
  }

  template<typename U>
  /* explicit */operator Publisher<U>(){
    return Publisher<U>(static_cast<Publisher<U>>(this));
  }
};

}